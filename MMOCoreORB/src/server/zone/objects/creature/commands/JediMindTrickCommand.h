/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef JEDIMINDTRICKCOMMAND_H_
#define JEDIMINDTRICKCOMMAND_H_

#include "server/zone/objects/creature/events/StealthDelayEvent.h"

class JediMindTrickCommand : public ForcePowersQueueCommand {
public:

	JediMindTrickCommand(const String& name, ZoneProcessServer* server) : ForcePowersQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		
		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

		CreatureObject* player = cast<CreatureObject*>(creature);

		if (player->isRidingMount())
			return GENERALERROR;
			
		if (!player->checkCooldownRecovery("stealth_message")) {
			player->sendSystemMessage("Cannot stealth again so soon.");
			return GENERALERROR;
		}
		creature->updateCooldownTimer("stealth_message", 60 * 1000);
		
		Reference<StealthDelayEvent*> invisTask = new StealthDelayEvent(player);
		Reference<StealthDelayEvent*> appearTask = new StealthDelayEvent(player);

		creature->playEffect("clienteffect/pl_force_resist_disease_self.cef");
		creature->addPendingTask("stealthdelayevent", invisTask, 1600);
		
		creature->addPendingTask("appeardelayevent", appearTask, 11600);
		
		return SUCCESS;
	}

};

#endif //JEDIMINDTRICKCOMMAND_H_
