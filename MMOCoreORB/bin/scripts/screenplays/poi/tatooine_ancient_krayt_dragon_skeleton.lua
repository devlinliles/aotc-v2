AncientKraytSkeletonScreenPlay = ScreenPlay:new {
	numberOfActs = 1,

}

registerScreenPlay("AncientKraytSkeletonScreenPlay", true) -- Core3 has this turned false. GY is also false. How scarce do we actually want loot to be?

function AncientKraytSkeletonScreenPlay:start()
	if (isZoneEnabled("tatooine")) then
		self:spawnMobiles()
	end
end

function AncientKraytSkeletonScreenPlay:spawnMobiles()

	spawnMobile("tatooine", "krayt_dragon_grand", 600, -4555.0, 49.3, -4459.2, -116, 0)
	spawnMobile("tatooine", "krayt_dragon_ancient", 600, -4713.1, 46.5, -4288.3, 50, 0)
	spawnMobile("tatooine", "giant_canyon_krayt_dragon", 600, -4669.8, 30.1, -4477.7, 5, 0)
	spawnMobile("tatooine", "canyon_krayt_dragon", 600, -4528.8, 28.3, -4302.4, 144, 0)
	spawnMobile("tatooine", "juvenile_canyon_krayt_dragon", 300, -4521.3, 27.2, -4298.2, 144, 0)
	spawnMobile("tatooine", "krayt_dragon_adolescent", 600, -4747.2, 32.5, -4424.8, -91, 0)

--[[
	-- Additional Krayts...
spawnMobile("tatooine", "krayt_dragon_adolescent", 600, -4357.66, 53.4281, -4662.03, 257, 0)
spawnMobile("tatooine", "krayt_dragon_grand", 600, -4316.79, 22.4563, -4197.57, 281, 0)
spawnMobile("tatooine", "giant_canyon_krayt_dragon", 600, -4506.46, 42.6573, -4038.62, 213, 0)
spawnMobile("tatooine", "krayt_dragon_adolescent", 600, -4864.03, 27.6843, -4042.61, 190, 0)
spawnMobile("tatooine", "krayt_dragon_adolescent", 600, -4546.06, 45.7002, -4941.21, 146, 0)
spawnMobile("tatooine", "giant_canyon_krayt_dragon", 600, -4536.3, 47.8401, -4977.65, 146, 0)
spawnMobile("tatooine", "krayt_dragon_adolescent", 600, -4312.84, 25.0206, -5008.96, 269, 0)
spawnMobile("tatooine", "krayt_dragon_ancient", 600, -4001.09, 22.1251, -4272.67, 153, 0)

-- Nearby Pod
spawnMobile("tatooine", "krayt_dragon_ancient", 600, -6197.31, 31.2349, -3181.88, 179, 0)
spawnMobile("tatooine", "krayt_dragon_adolescent", 600, -6582.19, 81.8896, -3621.37, 104, 0)
]]-- 

end
