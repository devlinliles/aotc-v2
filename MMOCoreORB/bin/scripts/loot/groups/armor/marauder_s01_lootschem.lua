marauder_s01_lootschem = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		{itemTemplate = "marauder_s01_chest_schematic", weight = 1111111},
		{itemTemplate = "marauder_s01_helm_schematic", weight = 1111112},
		{itemTemplate = "marauder_s01_leggings_schematic", weight = 1111111},
		{itemTemplate = "marauder_s01_boots_schematic", weight = 1111111},
		{itemTemplate = "marauder_s01_gloves_schematic", weight = 1111111},
		{itemTemplate = "marauder_s01_bracer_l_schematic", weight = 1111111},
		{itemTemplate = "marauder_s01_bracer_r_schematic", weight = 1111111},
		{itemTemplate = "marauder_s01_bicep_l_schematic", weight = 1111111},
		{itemTemplate = "marauder_s01_bicep_r_schematic", weight = 1111111},
	}
}


addLootGroupTemplate("marauder_s01_lootschem", marauder_s01_lootschem)
