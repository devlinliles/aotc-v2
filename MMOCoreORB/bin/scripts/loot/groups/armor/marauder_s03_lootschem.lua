marauder_s03_lootschem = {
	description = "",
	minimumLevel = 0,
	maximumLevel = -1,
	lootItems = {
		{itemTemplate = "marauder_s03_chest_schematic", weight = 1111111},
		{itemTemplate = "marauder_s03_helm_schematic", weight = 1111112},
		{itemTemplate = "marauder_s03_leggings_schematic", weight = 1111111},
		{itemTemplate = "marauder_s03_boots_schematic", weight = 1111111},
		{itemTemplate = "marauder_s03_gloves_schematic", weight = 1111111},
		{itemTemplate = "marauder_s03_bracer_l_schematic", weight = 1111111},
		{itemTemplate = "marauder_s03_bracer_r_schematic", weight = 1111111},
		{itemTemplate = "marauder_s03_bicep_l_schematic", weight = 1111111},
		{itemTemplate = "marauder_s03_bicep_r_schematic", weight = 1111111},
	}
}


addLootGroupTemplate("marauder_s03_lootschem", marauder_s03_lootschem)
