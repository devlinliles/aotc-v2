--Automatically generated by SWGEmu Spawn Tool v0.12 loot editor.

bh_bracer_r_schematic = {
	minimumLevel = 0,
	maximumLevel = -1,
	customObjectName = "",
	directObjectTemplate = "object/tangible/loot/loot_schematic/bounty_hunter_bracer_r_schematic.iff",
	craftingValues = {
	},
	customizationStringNames = {},
	customizationValues = {}
}

addLootItemTemplate("bh_bracer_r_schematic", bh_bracer_r_schematic)
