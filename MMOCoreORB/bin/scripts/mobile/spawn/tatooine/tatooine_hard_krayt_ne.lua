tatooine_hard_krayt_ne = {
	lairSpawns = {
		{
			lairTemplateName = "tatooine_canyon_krayt_dragon_lair_neutral_large_boss_01",
			spawnLimit = 2,
			minDifficulty = 122,
			maxDifficulty = 276,
			numberToSpawn = 0,
			weighting = 4,
			size = 35
		},
		--[[{
			lairTemplateName = "tatooine_canyon_krayt_dragon_lair_neutral_large",
			spawnLimit = 2,
			minDifficulty = 122,
			maxDifficulty = 276,
			numberToSpawn = 0,
			weighting = 1,
			size = 35
		},]]
		{
			lairTemplateName = "tatooine_canyon_krayt_dragon_lair_neutral_large_boss_02",
			spawnLimit = 2,
			minDifficulty = 275,
			maxDifficulty = 304,
			numberToSpawn = 0,
			weighting = 2,
			size = 35
		},
		--[[{
			lairTemplateName = "tatooine_giant_canyon_krayt_dragon_lair_neutral_large",
			spawnLimit = 1,
			minDifficulty = 275,
			maxDifficulty = 304,
			numberToSpawn = 0,
			weighting = 1,
			size = 35
		},]]
		{
			lairTemplateName = "tatooine_krayt_dragon_ancient_neutral_none",
			spawnLimit = 1,
			minDifficulty = 320,
			maxDifficulty = 350,
			numberToSpawn = 0,
			weighting = 1,
			size = 35,
		},
	}
}

addSpawnGroup("tatooine_hard_krayt_ne", tatooine_hard_krayt_ne);
