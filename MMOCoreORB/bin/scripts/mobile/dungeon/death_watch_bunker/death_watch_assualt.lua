death_watch_assualt = Creature:new {
	objectName = "@mob/creature_names:death_watch_elite_ranged",
	mobType = MOB_NPC,
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	socialGroup = "death_watch",
	faction = "",
	level = 300,
	chanceHit = 1.25,
	damageMin = 2040,
	damageMax = 2900,
	baseXp = 22314,
	baseHAM = 25000,
	baseHAMmax = 50142,
	armor = 2,
	resists = {80,80,65,25,85,80,60,60,150},
	-- kin,eng,elect,stun,blast,heat,cold,acid,lightsaber
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,
	scale = 1.00,

	templates = {"object/mobile/dressed_death_watch_gold.iff"},
	lootGroups = {
		{
				groups = {
				{group = "death_watch_bunker_ingredient_protective",  chance = 500000},
				{group = "death_watch_bunker_ingredient_binary",  chance = 500000},
				{group = "clothing_attachments", chance = 4500000},
				{group = "armor_attachments", chance = 4500000},
			},
			lootChance = 4500000
		},
			{
				groups = {
					{group = "death_watch_bunker_commoners",   chance = 5000000},
					{group = "death_watch_weapon_components",   chance = 5000000},
				},
				lootChance = 2000000
			},
			{
				groups = {
					{group = "mandalorian_common",   chance = 5000000},
					{group = "death_watch_weapon_components",   chance = 5000000},
				},
				lootChance = 2000000
			},
		},
	primaryWeapon = "assault_weapons",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",

	primaryAttacks = merge(carbineermaster,mastermarksman),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(death_watch_assualt,"death_watch_assualt")
