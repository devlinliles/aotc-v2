shadow_collective_ig = Creature:new {
	customName = "an IG Lancer droid",
	mobType = MOB_ANDROID,
	socialGroup = "shadow_collective",
	faction = "",
	level = 303,
	chanceHit = 1.55,
	damageMin = 1270,
	damageMax = 2950,
	baseXp = 28130,
	baseHAM = 280000,
	baseHAMmax = 425714,
	armor = 3,
	resists = {60,60,60,45,30,50,35,30,130},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY + NOBLIND + NODIZZY,
	creatureBitmask = KILLER,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,
	scale = 1.25,

	templates = {"object/mobile/ig_88.iff"},
	lootGroups = {
    {
        groups = {
            {group = "clothing_attachments", chance = 5000000},
            {group = "armor_attachments", chance = 5000000},
        },
        lootChance = 8000000
    },
		{
			groups = {
				{group = "underworld_common", chance = 6000000},
				--{group = "jetpack_base", chance = 2000000},
				{group = "powerplants", chance = 4000000},
			},
			lootChance = 4500000
		},

	},

	primaryWeapon = "ig_lancer_weapons",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",

	primaryAttacks = merge(pikemanmaster,brawlermaster,riflemanmaster),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(shadow_collective_ig, "shadow_collective_ig")
